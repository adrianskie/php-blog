<?php
$lang['delete_confirmation'] = "This will permanently delete the account. Action is not reversable?";
$lang['delete_confirmation_comment'] = "This will permanently delete the comment. Action is not reversable?";
?>