<?php
class MY_User extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		//$this->load->view('include/header');
		
		// If user already logged in
		if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');
			
			$data['id'] = $session_data['id'];
			$data['username'] = $session_data['username'];
			$data['first_name'] = $session_data['first_name'];
			$data['last_name'] = $session_data['last_name'];
			$data['level'] = $session_data['level'];

			return $data;
			//$this->load->view('home_view', $data);
			//$this->load->view('include/footer');
		}
	else
		{
			//If no session, redirect to login page
			// and exit it
			$this->session->set_flashdata('notice', 'Permission denied.' );
			redirect('login', 'refresh');
			exit();
		}
	}
}
