<?php
if (!defined('BASEPATH')) die();
class Frontpage extends Main_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('user_model','',TRUE);
   $this->load->model('blog_model','',TRUE);
 }


   public function index()
	{
		
		// Check if the user has already logged in
		if($this->session->userdata('logged_in') != false)
	   {
			redirect(base_url('index.php/home'));		
		}
	   // Show them registration form
	   else{
			$data['url_link'] = $this->uri->segment(1, 0);
			$this->load->view('include/header',$data);
			$this->load->helper(array('form'));
			
			$data['latestBlog'] = $this->blog_model->getLatestBlog();
			
			
			$this->load->view('frontpage_view',$data);
			$this->load->view('include/footer');
		}

	}
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
