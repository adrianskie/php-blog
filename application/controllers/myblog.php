<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Myblog extends MY_User {

 function __construct()
 {
   parent::__construct();
   $this->load->model('blog_model','',TRUE);
   $this->load->helper('form');
   $this->load->library('form_validation');
   
   // Keep the session goin
    if(parent::__construct())
   {
   // Make a global variable
   $this->SESSION_DATA = parent::__construct();
   }
 }

 function share()
 {
 	$data['result'] = "";
	$this->form_validation->set_rules('inputTitle', 'Title', 'required');
	$this->form_validation->set_rules('inputContent', 'Content', 'required');
	// Get all the blog of base on the userid
	$data['query'] = $this->blog_model->getBlog($this->SESSION_DATA['id']);

	if ($this->form_validation->run() === FALSE)
	{
		$data['result'] = "Something goes wrong with the validation.";
		$this->load->view('include/header',$data);

		$this->load->view('blog_view');
		$this->load->view('include/footer');
	
	}
	else
	{
		$this->blog_model->insertBlog($this->SESSION_DATA['id']);
		
		$this->session->set_flashdata('notice', '<p>Comment has been added.</p>' );
		
        redirect(base_url('index.php/myblog/'));
		
		//$data['result'] = "Comment has been added.";
		//$this->blog_model->insertBlog($this->SESSION_DATA['id']);
		//$this->load->view('include/header');

		//$this->load->view('blog_view', $data);
		//$this->load->view('include/footer');
	}
 
 }

 function index()
 {
	$data['result'] = "";
   $this->load->view('include/header',$this->SESSION_DATA);
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
	 
     $data['username'] = $session_data['username'];
	 $data['first_name'] = $session_data['first_name'];
	 $data['last_name'] = $session_data['last_name'];
	 $data['level'] = $session_data['level'];
	 
	 // Get all the blog of base on the userid
	 $data['query'] = $this->blog_model->getBlog($this->SESSION_DATA['id']);
	 
     $this->load->view('blog_view', $data);
	 $this->load->view('include/footer');
	 
	 
   }
   else
   {
     //If no session, redirect to login page
	 $this->load->view('permission_view');
	 $this->load->view('include/footer');
   }
 }
 
 function edit()
 {
	// Get the blog id of edit
	$blog_id = $this->uri->segment(3);
	$data['blog_info'] = $this->blog_model->getBlogInfo($blog_id);
	
	$data['result'] = "";
	$this->load->view('include/header',$this->SESSION_DATA);
	if($this->session->userdata('logged_in'))
   {
		$session_data = $this->session->userdata('logged_in');

		$data['username'] = $session_data['username'];
		$data['first_name'] = $session_data['first_name'];
		$data['last_name'] = $session_data['last_name'];
		$data['level'] = $session_data['level'];

		// Get all the blog of base on the userid
		$data['query'] = $this->blog_model->getBlog($this->SESSION_DATA['id']);

		$this->load->view('blog_edit_view', $data);
		$this->load->view('include/footer');
   }
   else
   {
     //If no session, redirect to login page
	 $this->load->view('permission_view');
	 $this->load->view('include/footer');
   }
 }
 
 function edit_form()
 {
 	$data['result'] = "";
	$this->form_validation->set_rules('inputTitle', 'Title', 'required');
	$this->form_validation->set_rules('inputContent', 'Content', 'required');
	// Get all the blog of base on the userid
	$data['query'] = $this->blog_model->getBlog($this->SESSION_DATA['id']);

	if ($this->form_validation->run() === FALSE)
	{
		$data['result'] = "Something goes wrong with the validation.";
		$this->load->view('include/header',$data);

		$this->load->view('blog_view');
		$this->load->view('include/footer');
	
	}
	else
	{
		$this->blog_model->updateBlog();
		
		$this->session->set_flashdata('notice', '<p>Comment has been edited.</p>' );
		
        redirect(base_url('index.php/myblog/'));
		
		//$data['result'] = "Comment has been added.";
		//$this->blog_model->insertBlog($this->SESSION_DATA['id']);
		//$this->load->view('include/header');

		//$this->load->view('blog_view', $data);
		//$this->load->view('include/footer');
	}
 
 }

 
 	function delete()
	{
		$example_id = $this->uri->segment(3);
		$this->blog_model->delete($example_id);
		
		/* 
		@author Adrian
		@date 18/11/2013
		@description add message when the user gets disconnected
		@using flashdata to load message once the function gets executed.
		@there should be if statement to check wheather notice variable is set or not.
		*/
		$this->session->set_flashdata('notice', '<p>Blog has been sucessfully deleted.</p>' );
		
        redirect(base_url('index.php/myblog/'));
	}

 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('', 'refresh');
 }

}


