<?php
class Blog extends MY_User {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model','',TRUE);
		$this->load->model('blog_model','',TRUE);
		
		/* @author Adrian Roy Baguio
		   @date 11/26/13
		   @description Load form and validation as we will need this for posting comments.
		*/
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		/* @author Adrian Roy Baguio
		   @date 18/11/2013
		   @description Added language so someone can translate the code in different language in the future.
		   
		*/
		$this->lang->load('main', 'english');
		
		$this->SESSION_DATA = parent::__construct();
	}
	
	 function index()
	{
	   $data = parent::__construct();
	   $this->load->helper('url');
	   $this->load->view('include/header',$data);
	   
	   $session_data = $this->session->userdata('logged_in');
	   
	   if($this->session->userdata('logged_in') &&  $session_data['level'] == 1)
	   {
		 $session_data = $this->session->userdata('logged_in');
		 $data['query'] = $this->user_model->fetchAccounts();
		 $this->load->view('blog_full_view',$data);
		 $this->load->view('include/footer');
	   }
	   else
	   {
		 //If no session, redirect to login page
		 //redirect('', 'refresh');
		 
		 $this->load->view('permission_view');
		 $this->load->view('include/footer');
	   }
	}
	 function show()
	 {
		$data = parent::__construct();
		$this->load->view('include/header',$data);
		$blog_id = $this->uri->segment(3);
		$data['query_blog'] = $this->blog_model->getBlogInfo($blog_id);
		
		// This will check if the checker is the owner of the blog, and give ability to delete comments. Otherwise no.
		if($data['query_blog'][0]->id == $this->SESSION_DATA['id']){
			$data['ownership'] = true;
		}
		$data['query_comments'] = $this->blog_model->getComment($blog_id);
		$this->load->view('blog_full_view',$data);
		
		$this->load->view('include/footer');
	 }
	 
	 function comment()
	 {
		$this->form_validation->set_rules('inputComment', 'Comment', 'required');
		$this->form_validation->set_rules('blogID', 'Something is missing', 'required');
		if ($this->form_validation->run() === FALSE)
		{
		$data['result'] = "Something goes wrong with the validation.";
		$this->load->view('include/header',$data);

		$this->load->view('blog_view');
		$this->load->view('include/footer');
		}
		else
		{	
			// insertComment($userid,$blog_id)
			$this->blog_model->insertComment($this->SESSION_DATA['id']);
			
			$this->session->set_flashdata('notice', "Comment has been added.");	
			
			// Link + the last 3rd URI
			redirect(base_url('index.php/blog/show/' . $this->input->post('blogID')));
		}
	 }
	 
	 function deleteComment()
	 {
		$example_id = $this->uri->segment(3);
		$this->blog_model->deleteComment($example_id);

		$this->session->set_flashdata('notice', 'Comment has been succesfully deleted.' );
		
        //redirect(base_url('index.php/blog/show/' . $example_id));
		redirect($this->input->get('last_url'));
	 }
	
}