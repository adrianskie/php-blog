<?php
if (!defined('BASEPATH')) die();
class Login extends Main_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('user_model','',TRUE);
	$this->load->helper(array('form', 'url'));
	$this->load->library('form_validation');
 }


   public function index()
	{
		
		// Check if the user has already logged in
		if($this->session->userdata('logged_in') != false)
	   {
			$data = parent::__construct();
			$this->load->view('include/header',$data);
			$this->load->view('login_view');
			$this->load->view('include/footer');	
		}
	   // Show them registration form
	   else{
			$data['url_link'] = $this->uri->segment(1, 0);
			$this->load->view('include/header',$data);
			$this->load->helper(array('form'));

			
			
			$this->load->view('login_view',$data);
			$this->load->view('include/footer');
		}

	}
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
