<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
session_start(); //we need to call PHP's session object to access it through CI
class Home extends MY_User {
 function __construct()
 {
   parent::__construct();
   $this->load->model('user_model','',TRUE);
   $this->load->model('blog_model','',TRUE);
   $this->DATA_SESSION = parent::__construct();
 }
 
 function index()
 {
	/*
	$data = parent::__construct();
	$this->load->view('include/header',$data);
	$this->load->view('home_view');
	$this->load->view('include/footer');
	*/
	if($this->DATA_SESSION)
	{
		$data = $this->DATA_SESSION;
	};
	$data['url_link'] = $this->uri->segment(1, 0);
	$this->load->view('include/header',$data);
	$this->load->helper(array('form'));
	
	$data['latestBlog'] = $this->blog_model->getLatestBlog();
	
	$this->load->view('frontpage_view',$data);
	$this->load->view('include/footer');
	
	
 }

 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('', 'refresh');
 }

}

