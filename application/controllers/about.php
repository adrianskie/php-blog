<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class About extends Main_Controller {
 function __construct()
 {
   parent::__construct();
 }
 
 function index()
 {
	$data = parent::__construct();
	$this->load->view('include/header');
	$this->load->view('about_view');
	$this->load->view('include/footer');
 }
 }

