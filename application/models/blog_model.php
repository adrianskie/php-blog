<?php
/* 
@Author Adrian Roy Antonio Baguio
@Date 20/11/13
@Desc - added model for blog CRUD
*/
class Blog_model extends CI_Model{
	
	function getLatestBlog()
	{
	
		/*
		$this -> db -> select('*');
		$this -> db -> from('blog');
		$this -> db -> order_by("date_posted", "desc"); 
		$this->db->limit(2);
		*/
		
		/*
		SELECT u.first_name, u.last_name, b.subject, b.content, b.date_posted
		FROM users as u, blog as b
		WHERE u.id = b.user_id 
		ORDER BY date_posted
		DESC
		LIMIT 2;
		*/
		
		$this->db->select('u.first_name, u.last_name, b.subject, b.content, b.date_posted, b.blog_id')
          ->from('users AS u, blog AS b')
          ->where('u.id = b.user_id');
		  $this -> db -> order_by("date_posted", "desc");
		  $this -> db -> limit(4);

		
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
			return $data;
		}
		return false;
	}

	function insertBlog($userid)
	{
		$this->load->helper('url');

		$data = array(
			'user_id' => $userid,
			'subject' => $this->input->post('inputTitle'),
			'content' => $this->input->post('inputContent'),
			'date_posted' => date("Y-m-d g:i:s", time())
		);

		return $this->db->insert('blog', $data);
	}
	
	function updateBlog()
	{
		$data = array(
               'subject' => $this->input->post('inputTitle'),
               'content' => $this->input->post('inputContent'),
            );

		$this->db->where('blog_id', $this->input->post('blog_id'));
		$this->db->update('blog', $data); 
	}
	
	function insertComment($userid)
	{
		$this->load->helper('url');

		$data = array(
			'user_id' => $userid,
			'blog_id' => $this->input->post('blogID'),
			'comment' => $this->input->post('inputComment'),
			'date' => date("Y-m-d g:i:s", time())
		);

		return $this->db->insert('comments', $data);
	}
	
	function getBlog($userid)
	{
		$this -> db -> select('*');
		$this -> db -> from('blog');
		$this->  db -> where('user_id =', $userid);
		//$this->  db->  group_by('blog_id','desc'); 
		$this->  db->  order_by('blog_id','desc');
		
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
			return $data;
		}
		return false;
	}
	
	function getComment($blog_id)
	{
	
	
		$this -> db -> select('c.comment_id, c.blog_id, c.user_id, c.date, c.comment, u.first_name, u.last_name');
		$this -> db -> from('comments AS c, users as u');
		$this->  db -> where('blog_id =', $blog_id);
		$this->  db -> where('c.user_id = u.id');
		//$this->  db->  group_by('blog_id','desc'); 
		$this->  db ->  order_by('comment_id','desc');
		
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
			return $data;
		}
		return false;
	}
	
	function getBlogInfo($blogid)
	{
		/*
		$this -> db -> select('*');
		$this -> db -> from('blog');
		$this->  db -> where('blog_id =', $blogid);
		//$this->  db->  group_by('blog_id','desc'); 
		//$this->  db->  order_by('blog_id','desc'); 
		*/
		
		$this->db->select('u.id ,u.first_name, u.last_name, b.subject, b.content, b.date_posted, b.blog_id')
		->from('users AS u, blog AS b')
		->where('b.blog_id =', $blogid)
		->where('b.user_id = u.id');
		$this -> db -> order_by("date_posted", "desc");
		$this -> db -> limit(1);
		
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
			return $data;
		}
		return false;
	}
	
		function delete($blog_id)
	{
		$this->db->delete('blog', array('blog_id' => $blog_id));
		//$this->db->where("blog_id", $this->$blog_id);
        //$this->db->delete("blog");
	}
		function deleteComment($comment_id)
	{
		$this->db->delete('comments', array('comment_id' => $comment_id));
	}
	
	
}