	<?php echo validation_errors(); ?>
   <div>
        <div class="row">
        <div class="col-md-4">
        
        </div>
        <div class="col-md-4">
	<?php echo form_open('verifylogin'); ?>
	<?php
		if ($this->session->flashdata('notice') != ''): 
		echo '<div id="notice" style="display:none">';
		echo '<span class="label label-danger">' . $this->session->flashdata('notice') . '</span>'; 
		echo '</div>';
		endif;
	?>
	<br>
  <div class="form-group">
    <label for="Username">User Name</label>
    <input type="text" class="form-control" id="username" name="username" placeholder="User Name">
  </div>
  <div class="form-group">
    <label for="Password">Password</label>
    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
  </div>
  
  <button type="submit" value="login" class="btn btn-primary btn-lg btn-block">Submit</button>
  <button type="reset" class="btn btn-primary btn-lg btn-block">Reset</button>
</form>
	<br>
	<script>
		$(document).ready(function() {
			 $("#notice").fadeIn(3000);
			});
	</script>
        </div>
        <div class="col-md-4">
        
        </div>
        </div>

		