
   <h1>Face Blog!</h1>
    <?php echo validation_errors(); ?>
	<?php echo form_open(base_url('index.php/myblog/edit_form')) ?>
  <div class="form-group has-success" style="width:500px; float:left;">
  <label class="control-label">What are you thinking?</label>
 
  <input type="hidden" id="blog_id" name="blog_id" value="<?php if(isset($blog_info)){echo $blog_info[0]->blog_id;}?>">
  <input name="inputTitle"  id="inputTitle" <?php if(isset($blog_info)){ ?> value="<?php /* This will put a the subject back if it exsist. */ echo $blog_info[0]->subject; ?>"<?php } ?> type="text" class="form-control" style="width:250px; margin-bottom:5px;" placeholder="Title">
  <textarea name="inputContent" id="inputContent" type="text" class="form-control" id="inputSuccess" style="resize:none; height:100px;"  placeholder="Tell us about it!"><?php /* This will put a content if it exsist. */ if(isset($blog_info)){ echo $blog_info[0]->content;  } ?></textarea>
  <!-- Indicates a successful or positive action -->
	<button type="submit" class="btn btn-success" style="float:right;">Submit</button>
	<?php
		if ($this->session->flashdata('notice') != ''): 
		echo $this->session->flashdata('notice'); 
		endif;
	?>
	</div>
	
	
	  <div class="form-group has-success" style="width:500px; float:right; padding-left:50px;">
		<?php if($query){ foreach ($query as $blog):?>		
		<div class="panel panel-warning">
		<div class="panel-heading"><?php echo $blog->subject?> <a href="<?php echo base_url('index.php/myblog/delete/' . $blog->blog_id) ?>""><span class="glyphicon glyphicon-remove" style="float:right; margin-left:5px;"></span></a><a href="<?php echo base_url('index.php/myblog/edit/' . $blog->blog_id) ?>"><span class="glyphicon glyphicon-edit" style="float:right;"></span></a></div>
		<div class="panel-body"><?php echo $blog->content?></div>
		</div>
		<h6>Posted on: <?php echo $blog->date_posted?></h6>
		<hr> 
        <?php endforeach;
		} else {
			?>
				<div class="alert alert-warning">You have not posted yet at this time.</div>
			<?php
		}
		?>
	
	  </div>
   
   <!-- <a href="home/logout">Logout</a> -->