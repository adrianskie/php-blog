  <?php echo validation_errors(); ?>
  <?php echo form_open(base_url('index.php/blog/comment')) ?>
  <div class="jumbotron">
  <h1><?php print $query_blog[0]->subject;?></h1>
  <p><?php print $query_blog[0]->content;?></p>
  <span class="glyphicon glyphicon-user"></span><?php echo "By: " . $query_blog[0]->first_name." ".$query_blog[0]->last_name; ?>
  <p><button type="submit" id="comment"  class="btn btn-primary btn-lg" role="button" >Comment</button></p>
  <textarea name="inputComment" id="inputComment" type="text" class="form-control" id="inputSuccess" style="resize:none; height:100px;"  placeholder="Comment..."></textarea>
  <input name="blogID" id="blogID" type="hidden" value="<?php echo $this->uri->segment(3); ?>"></input>
  </div>
  </form>
	<?php 
	if ($this->session->flashdata('notice') != ''): 
	echo '<div id="notice" style="display:none">';
	echo '<span class="label label-success">' . $this->session->flashdata('notice') . '</span>'; 
	echo '</div>';
	?>
	<script>
		$('html, body').animate({scrollTop: $("body").height()}, 800);
	</script>
	<?php
	endif;
?>

  <?php
	if($query_comments == false){ ?>
		<div class="alert alert-warning">There are no comments yet for this blog.</div>
	<?php } else { 
		foreach ($query_comments as $comment): ?>	
			<div class="panel panel-primary">
			<div class="panel-heading">
			<h3 class="panel-title">By: <?php echo $comment->first_name . " " . $comment->last_name?></h3>
			</div>
			<div class="panel-body"><?php echo $comment->comment?>
			
			<?php if(isset($ownership)){?>
			<a href="<?php echo base_url('index.php/blog/deleteComment/' . $comment->comment_id . "?last_url=" . urlencode(current_url())) ?>" onclick="return confirm('<?php print $this->lang->line('delete_confirmation'); ?>');"><span class="glyphicon glyphicon-remove"  style="float:right;"></span></a>
			<?php }?>
			
			</div>
			</div>

		<?php endforeach;?>

			
	<?php }
	?>
	

<script>
// @date - 11/26/13
// @author - Adrian Roy A Baguio
// @Desc- added javascript effect when comment link is clicked.
/*
$("a#comment").click(function() {
  alert( "Handler for .click() called." );
});
*/
$(document).ready(function() {
	 $("#notice").fadeIn(3000);
	});
</script>
