<!DOCTYPE html>
<html lang="en">
<head>
   <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta name="description" content="">
   <meta name="keywords" content="">
   <meta name="author" content="">

   <title>Face U</title>

   <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
   <link href="<?php echo base_url('assets/css/font-awesome.css') ?>" rel="stylesheet">
   <link href="<?php echo base_url('assets/css/custom.css') ?>" rel="stylesheet">
</head>
<body>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<!--
@author Adrian Roy Antonio Baguio
@date 20/11/13
@Description adding navigation on top to reduce coding redundancy
 -->

<!-- @Author Xin
	 @Sub author, Adrian, Jiji
	 @Date 22/11/2013
	 @Description: Apply Xin's template
	 -->
  <div class="container">
	<div class="row">
		<div class="col-md-4">
			<img src="<?php echo asset_url()?>/blog/images/logo.jpg" class="img-responsive" alt="Responsive image">
		</div>
		<div class="col-md-8">
			<h1 class="text-primary">Welcome to our php project</h1>
		</div>
    </div>
    <div class="row">
		<div class="col-md-12">
			<nav class="navbar navbar-default" role="navigation">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">BLOG</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="<?php echo base_url('index.php/frontpage')?>">Home</a></li>
					<?php 
					if(!isset($username)) {?>
					<li><a href="<?php echo base_url('index.php/register/create'); ?>">Register</a></li>
					<li><a href="<?php echo base_url('index.php/login'); ?>">login</a></li>
					<?php }
					// If show admin 
					else if($level == 1){?>
					<li><a href="<?php echo base_url('index.php/users')?>">Users</a></li>
					<?php } 
					// Else users
					else {?>
					<li><a href="<?php echo base_url('index.php/myblog')?>">MyBlog</a></li>
					<?php }?>
				</ul>
				<form class="navbar-form navbar-left" role="search">
					<div class="form-group">
					<input type="text" class="form-control" placeholder="Search">
					</div>
					<button type="submit" class="btn btn-default">Submit</button>
				</form>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="<?php echo base_url('index.php/about')?>">About</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
			</nav>
		</div>
    </div>

