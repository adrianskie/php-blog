<!DOCTYPE html>
<html>
  <head>
    <title>User Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="../dist/css/bootstrap.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="http://cdn.bootcss.com/html5shiv/3.7.0/html5shiv.min.js"></script>
        <script src="http://cdn.bootcss.com/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
    <div class="row">
    	<div class="col-md-12">
        <nav class="navbar navbar-default" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="#">BLOG</a>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
      <li><a href="index.html">Home</a></li>
    </ul>
  </div><!-- /.navbar-collapse -->
</nav>
        </div>
        </div>
       
     <div class="alert alert-danger">Sorry you cannot access the page ,please login or register first</div>
     <div class="row">
     	<div class="col-md-3">
        </div>
        <div class="col-md-3">
        	<a class="btn btn-default btn-primary btn-lg btn-block" href="login.php" role="button">Login</a>
        </div>
        <div class="col-md-3">
        	<a class="btn btn-default btn-primary btn-lg btn-block" href="register.php" role="button">Register</a>
        </div>
        <div class="col-md-3">
        </div>
     </div>
    </div>

   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../dist/js/jquery-1.10.2.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
     <script src="../dist/js/bootstrap.min.js"></script>
  </body>
</html>