-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 22, 2013 at 03:39 AM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `php_blog`
--
CREATE DATABASE IF NOT EXISTS `php_blog` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `php_blog`;

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE IF NOT EXISTS `blog` (
  `blog_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Blog id',
  `user_id` int(11) NOT NULL COMMENT 'Users who own the blog',
  `subject` varchar(255) NOT NULL,
  `content` varchar(1000) NOT NULL,
  `date_posted` datetime NOT NULL,
  PRIMARY KEY (`blog_id`,`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`blog_id`, `user_id`, `subject`, `content`, `date_posted`) VALUES
(3, 0, '', '', '2013-11-20 00:00:00'),
(4, 1, 'wadawdawdd', 'awdwad', '2013-11-21 04:23:35'),
(5, 1, 'This is my title', 'Thats about it!', '2013-11-21 04:23:51'),
(6, 0, 'awdwa', 'dawdawdwad', '2013-11-21 04:28:21'),
(7, 0, 'awdawd', 'adawdawd', '2013-11-21 04:28:24'),
(8, 0, 'awdaw', 'dwadwadwad', '2013-11-21 04:29:05'),
(37, 134, 'Toronto people', 'oh my godd!', '2013-11-21 07:14:05'),
(41, 133, 'buahahaha', 'this is my blog bitch', '2013-11-21 04:27:18'),
(42, 145, 'Test for jiji', 'I''m Jiji who are you?', '2013-11-21 04:31:58'),
(43, 145, 'Jiji', 'buahahaha', '2013-11-21 04:33:23'),
(44, 146, 'This is my blog bitch!', 'Buahahhaa!', '2013-11-21 05:54:46'),
(45, 133, 'wahhah', 'wahahaa', '2013-11-21 08:07:56');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `comment` varchar(255) NOT NULL,
  PRIMARY KEY (`comment_id`,`blog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_address` varchar(35) NOT NULL,
  `password` varchar(35) NOT NULL,
  `first_name` varchar(15) NOT NULL,
  `last_name` varchar(15) NOT NULL,
  `level` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=147 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email_address`, `password`, `first_name`, `last_name`, `level`) VALUES
(133, 'admin', '123', 'Adrian', 'Baguio', '1'),
(145, 'Jiji@yahoo.com', '123', 'Jiji', 'Canayong', NULL),
(146, 'albert_cute09@yahoo.com', '123', 'Albert', 'Baguio', NULL);
--
-- Database: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test`;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
